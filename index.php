<?php
require('Animal.php');
require('Ape.php');
require('Frog.php');

$sheep = new Animal("shaun");

echo $sheep->get_name() . "<br>"; // "shaun"
echo $sheep->get_legs() . "<br>"; // 2
echo $sheep->get_cold_blooded() . "<br>"; // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$sungokong = new Ape("kera sakti");
echo $sungokong->yell() . "<br>"; // "Auooo"

$kodok = new Frog("buduk");
echo $kodok->jump() . "<br>"; // "hop hop"

?>