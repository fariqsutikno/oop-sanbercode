<?php
class Animal {
		var $name;
        
		function __construct($name) {
            $this->legs = 2;
            $this->cold_blooded = 'false';	
            $this->name = $name;	
		}		
 
		// function set_name($new_name) {
		//  	 $this->name = $new_name;
		// }	
 
		function get_name() {		
		 	 return $this->name;		
		 }
		function get_legs() {		
		 	 return $this->legs;		
		 }
		function get_cold_blooded() {		
		 	 return $this->cold_blooded;		
		 }
                // function __destructor(){
                //          echo "<p>end class</p>";
                // }		
 
    }	 
    
?>